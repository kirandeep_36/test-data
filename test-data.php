<?php

/* Plugin Name:Test Data
  Description:WCL Widget  plugin helps you to integrate WCL Widget in to WordPress site.
  Author: WCL
  Author URI:http://worldcastlive.com/
  Version: 1.2.0
  
 */
/* Developer :Prashant Walke(Perennial System) */

/* Pusing plugin Update */
require 'plugin-update-checker/plugin-update-checker.php';

//======= Update function =========
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://bitbucket.org/kirandeep_36/test-data/',
    __FILE__,
    'wcl_widget.php'
);